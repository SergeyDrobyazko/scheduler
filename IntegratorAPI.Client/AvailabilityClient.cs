﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntegratorAPI.Client
{
    public class AvailabilityClient
    {
        public string BaseUri { get; set; }

        public AvailabilityClient()
        {
            this.BaseUri = "http://localhost:26357/";
        }

        public Task<IRestResponse> PushAvailability(int propertyId)
        {
            var client = new RestSharp.RestClient(BaseUri);

            RestRequest request = new RestRequest("api/PushAvailability", Method.GET);

            request.AddParameter("id", propertyId);

            var response = client.ExecuteTaskAsync(request);

            return response;
        }
    }
}
