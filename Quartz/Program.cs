﻿using Quartz.Impl;
using Quartz.Jobs;
using Quartz.Logging;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quartz
{
    class Program
    {
        static void Main(string[] args)
        {
            LogProvider.SetCurrentLogProvider(new ConsoleLogProvider());

            RunProgramRunExample().GetAwaiter().GetResult();
        }

        private static async Task RunProgramRunExample()
        {
            try
            {
                NameValueCollection props = new NameValueCollection
                {
                    { "quartz.scheduler.instanceName", "quartz"},
                    { "quartz.serializer.type", "json" },
                    { "quartz.jobStore.type", "Quartz.Impl.AdoJobStore.JobStoreTX, Quartz" },
                    { "quartz.jobStore.useProperties", "true"},
                    { "quartz.jobStore.dataSource", "default"},
                    { "quartz.jobStore.tablePrefix", "QRTZ_"},
                    {
                        "quartz.dataSource.default.connectionString",
                        "Server=DESKTOP-GRBB7IF\\MSSQLEXPRESS;Database=quartz;Trusted_connection=true"
                    },
                    { "quartz.dataSource.default.provider", "SqlServer"},
                    { "quartz.threadPool.threadCount", "3" }
                };
                //// grab the Scheduler instance from the Factory

                StdSchedulerFactory factory = new StdSchedulerFactory(props);
                IScheduler scheduler = await factory.GetScheduler();

                // and start it off
                await scheduler.Start();

                // define the job and tie it to our HelloJob class
                IJobDetail job = JobBuilder.Create<PushAvailability>()
                    .WithIdentity("pushAvailabilityJob1", "integrationOperationsGroup")
                    .Build();
                
                // Trigger the job to run now, and then repeat every 10 seconds
                ITrigger trigger = TriggerBuilder.Create()
                    .WithIdentity("trigger181", "integrationOperationsGroup")
                    .StartNow()
                    .WithSimpleSchedule(x => x
                        .WithIntervalInSeconds(10)
                        .RepeatForever())
                    .Build();

                // Tell quartz to schedule the job using our trigger
                await scheduler.ScheduleJob(job, trigger);

                // some sleep to show what's happening

                await Task.Delay(TimeSpan.FromSeconds(60));

                // and last shut down the scheduler when you are ready to close your program
                await scheduler.Shutdown();
            }
            catch (SchedulerException se)
            {
                Console.WriteLine(se);
            }
        }
    }

    // simple log provider to get something to the console
    class ConsoleLogProvider : ILogProvider
    {
        public Logger GetLogger(string name)
        {
            return (level, func, exception, parameters) =>
            {
                if (level >= LogLevel.Info && func != null)
                {
                    Console.WriteLine("[" + DateTime.Now.ToLongTimeString() + "] [" + level + "] " + func(), parameters);
                }
                return true;
            };
        }

        public IDisposable OpenNestedContext(string message)
        {
            throw new NotImplementedException();
        }

        public IDisposable OpenMappedContext(string key, string value)
        {
            throw new NotImplementedException();
        }
    }
}

