﻿using IntegratorAPI.Client;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quartz.Jobs
{
    public class PushAvailability : IJob
    {
        private AvailabilityClient client = new AvailabilityClient();

        public Task Execute(IJobExecutionContext context)
        {
            var result = client.PushAvailability(78);
            
            return result;
        }
    }
}
